import torch
from torch import nn
from torch.utils.data import DataLoader
from torchvision import models, datasets
from torchvision.transforms import ToTensor
import matplotlib.pyplot as plt
import json


def zero_padding(kernel_size):
    return (kernel_size-1)/2


class FashionNetwork(nn.Module):
    def __init__(self):
        super(FashionNetwork, self).__init__()

        self.conv_relu_stack = nn.Sequential(
            nn.Conv2d(1, 5, kernel_size=3, stride=2, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),
        )
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(7*7*5, 200),
            nn.ReLU(),
            nn.Linear(200, 10),
        )

    def forward(self, x):
        outs = self.conv_relu_stack(x)
        logits = self.linear_relu_stack(outs.view(outs.size(0), -1))
        return logits


def train_loop(data_loader, model, loss_function, optimizer):
    model.train()
    n_samples = len(data_loader.dataset)
    n_batches = len(data_loader)
    n_correct = 0
    for batch, (data, labels) in enumerate(data_loader):
        # Feed data through network and compute loss.
        data = data.to("cuda")
        labels = labels.to("cuda")
        prediction = model(data)
        loss = loss_function(prediction, labels)

        # Zero gradients.
        optimizer.zero_grad()

        # Perform backpropagation and accumulate gradients.
        loss.backward()

        # Update network parameters.
        optimizer.step()
        n_correct += (
            (prediction.argmax(1) == labels)
                .type(torch.float)
                .sum()
                .item()
        )
    train_accuracy = n_correct / n_samples
    train_loss = loss / n_batches
    return train_accuracy, train_loss


def test_loop(data_loader, model, loss_function):
    n_samples = len(data_loader.dataset)
    n_batches = len(data_loader)
    loss, n_correct = 0, 0
    model.eval()
    with torch.no_grad():
        for data, labels in data_loader:
            # Feed data through network and accumulate loss.
            data = data.to("cuda")
            labels = labels.to("cuda")
            prediction = model(data)
            loss += loss_function(
                prediction, labels
            ).item()
            n_correct += (
                (prediction.argmax(1) == labels)
                    .type(torch.float)
                    .sum()
                    .item()
            )

    test_accuracy = n_correct / n_samples
    test_loss = loss / n_batches
    return test_accuracy, test_loss


def train_and_test_stats(model, train_data_loader, test_data_loader, loss_func, optimizer, n_epochs):
    train_accs = []
    test_accs = []
    for t in range(n_epochs):
        print(f"Epoch {t + 1:02}", end=" ", flush=True)
        train_acc, train_loss = train_loop(train_data_loader, model, loss_func, optimizer)
        test_acc, test_loss = test_loop(test_data_loader, model, loss_func)  #
        train_accs.append(train_acc)
        test_accs.append(test_acc)
    return train_accs, test_accs


if __name__ == "__main__":
    # define training parameters
    BATCH_SIZE = 64
    EPOCHS = 10

    training_data = datasets.FashionMNIST(
        root="D:/datasets/FashionMNIST",
        train=True,
        download=True,
        transform=ToTensor()
    )

    test_data = datasets.FashionMNIST(
        root="D:/datasets/FashionMNIST",
        train=False,
        download=True,
        transform=ToTensor()
    )

    train_data_loader = DataLoader(training_data, batch_size=BATCH_SIZE)
    test_data_loader = DataLoader(test_data, batch_size=BATCH_SIZE)

    loss_func = nn.CrossEntropyLoss()
    learning_rate = 1e-3

    # train using SGD
    model = FashionNetwork()
    model.cuda()
    optimizer_sgd = torch.optim.SGD(model.parameters(), lr=learning_rate)
    sgd_results = train_and_test_stats(model, train_data_loader, test_data_loader, loss_func, optimizer_sgd, EPOCHS)

    # train using Adam
    model = FashionNetwork()
    model.cuda()
    optimizer_adam = torch.optim.SGD(model.parameters(), lr=learning_rate)
    adam_results = train_and_test_stats(model, train_data_loader, test_data_loader, loss_func, optimizer_adam, EPOCHS)

    # display and store results
    print(sgd_results)
    print(adam_results)
    fig, ax = plt.subplots(1,2, sharey=True)
    ax[0].plot(sgd_results[0], color='green', label="train acc")
    ax[0].plot(sgd_results[1], color='blue', label="test acc")
    ax[0].set_title("SGD")
    ax[0].legend()
    ax[1].plot(adam_results[0], color='green', label="train acc")
    ax[1].plot(adam_results[1], color='blue', label="test acc")
    ax[1].set_title("adam")
    plt.show()
    fig.savefig('output/task1_2.png')
    results = {"adam": adam_results, "sgd": sgd_results}
    with open("output/data1.json", "w+") as file:
        json.dump(results, file)

    # task 1.2
    batch_sizes = [8, 16, 32, 64]
    results = {}
    for batch_size in batch_sizes:
        model = FashionNetwork()
        model.cuda()
        optimizer_adam = torch.optim.SGD(model.parameters(), lr=learning_rate)
        adam_results = train_and_test_stats(model, train_data_loader, test_data_loader, loss_func, optimizer_adam,
                                            EPOCHS)
        results[batch_size] = adam_results
    print(results)
    fig, ax = plt.subplots(1, len(batch_sizes), sharey=True)
    for i, (batch_size, result) in enumerate(results.items()):
        ax[i].plot(result[0], color='green', label="train acc")
        ax[i].plot(result[1], color='blue', label="test acc")
        ax[i].set_title(f"Batch Size: {batch_size}")
        ax[i].legend()
    plt.show()
    fig.savefig('output/task2_2.png')
    with open("output/data2.json", "w+") as file:
        json.dump(results, file)






