import torch
import torchvision
from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor
import numpy as np
from torch import Tensor
import matplotlib.pyplot as plt
import torch.nn.functional as F

from art.attacks.evasion import FastGradientMethod
from art.attacks.evasion import UniversalPerturbation
from art.estimators.classification import PyTorchClassifier
from art.attacks.evasion import ProjectedGradientDescentPyTorch
from art.attacks.evasion import Wasserstein
from art.attacks.evasion import NewtonFool
from art.attacks.evasion import ElasticNet

test_data = datasets.MNIST(
	root="data",
	train=False,
	download=True,
	transform=ToTensor(),
)

test_data_loader = DataLoader(test_data, batch_size=128) 

#simple CNN Model for Fashion MNIST (with 2 convolutional layers)
class MNISTNetwork(nn.Module):
	
    def __init__(self):
        super(MNISTNetwork, self).__init__()

        self.conv_1 = nn.Conv2d(in_channels = 1, out_channels = 16, kernel_size = 3, stride = 1)
        self.conv_2 = nn.Conv2d(in_channels = 16, out_channels = 32, kernel_size = 3, stride = 1)
        self.drop = nn.Dropout(0.25)
        self.lin1 = nn.Linear(in_features=32*24*24,out_features=10)   
	
    def forward(self, x):
        x = self.conv_1(x)
        x = F.relu(x)
        x = self.drop(x)

        x = self.conv_2(x)
        x = F.relu(x)
        x = self.drop(x)

        x = x.reshape(-1,32*24*24)
        x = self.lin1(x)


        return x

model_adversarial_trained = MNISTNetwork()
model_adversarial_trained.load_state_dict(torch.load("adversarial_model_5epochs.pt")) #import model 
model_adversarial_trained.eval()
model_standard_trained = MNISTNetwork()
model_standard_trained.load_state_dict(torch.load("no_adversarial_model_5epochs.pt")) #import model 
model_standard_trained.eval()
#input_shape=(26,26)
cuda = torch.device('cuda')

#tutorial attack from the exercises, only works with batch size 1, not used for results
def image_to_tensor(input_array):
    # Normalize input.
    input_array = input_array - [0.485, 0.456, 0.406]
    input_array = input_array / [0.229, 0.224, 0.225]
 
    # Turn from (width, height, channel) into (batch, channel, width, height) and convert to tensor.
    input_tensor = torch.Tensor((input_array.transpose([2, 0, 1]))[None, ...])
 
    return input_tensor

sample = next(iter(test_data_loader))
imgs, lbls = sample
#imgs = imgs.to("cuda")

#baseline accuracy test
def test_accuracy(model):
    #calculate accuracy of test images
    predictions = model(imgs)
    n_correct = 0
    i= 0
    for prediction in predictions:
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs)
    print("Accuracy on benign test examples: {}%".format(accuracy * 100))
    return accuracy*100

#targeted attack
def adversial_toolbox_targeted(model):
    classifier = PyTorchClassifier(
        model=model,
        #clip_values=(min_pixel_value, max_pixel_value),
        loss=torch.nn.CrossEntropyLoss(),
        optimizer=torch.optim.SGD(model.parameters(), lr=1e-3),
        input_shape=(1, 26, 26),
        nb_classes=10,
        device_type="cuda",
    )
    
    #produce adversarials
    attack = FastGradientMethod(estimator=classifier, eps=0.2, targeted = True)
    imgs_adv = attack.generate(x=Tensor.cpu(imgs).numpy(),y=Tensor([1 for l in lbls])) #attack for all labels = 1

    #calculate accuracy of adversarial images
    predictions = model(torch.from_numpy(imgs_adv).to("cuda"))
    n_correct = 0
    i= 0
    for prediction in predictions:
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs_adv)
    print("Accuracy on targeted adversarial test examples: {}%".format(accuracy * 100))
    return accuracy*100

#untargted attack
def adversial_toolbox_untargeted(model):
    classifier = PyTorchClassifier(
        model=model,
        #clip_values=(min_pixel_value, max_pixel_value),
        loss=torch.nn.CrossEntropyLoss(),
        optimizer=torch.optim.SGD(model.parameters(), lr=1e-3),
        input_shape=(1, 26, 26),
        nb_classes=10,
        device_type="cuda",
    )

    #produce adversarials
    attack = FastGradientMethod(estimator=classifier, eps=0.2)
    imgs_adv = attack.generate(x=Tensor.cpu(imgs).numpy())

    #calculate accuracy of adversarial images
    predictions = model(torch.from_numpy(imgs_adv).to("cuda"))
    n_correct = 0
    i= 0
    avg_certainty = 0
    for prediction in predictions:
        certainty = torch.nn.functional.softmax(prediction,dim = 0).max().item()
        avg_certainty += certainty
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs_adv)
    print("Accuracy on untargeted adversarial test examples: {}%".format(accuracy * 100))
    print("Average untargeted Certainty: {}%".format(avg_certainty/i*100))
    return accuracy*100

#universal attack
def adversial_toolbox_universal(model):
    classifier = PyTorchClassifier(
        model=model,
        #clip_values=(min_pixel_value, max_pixel_value),
        loss=torch.nn.CrossEntropyLoss(),
        optimizer=torch.optim.SGD(model.parameters(), lr=1e-3),
        input_shape=(1, 26, 26),
        nb_classes=10,
        device_type="cuda",
    )

    #produce adversarials
    attack = UniversalPerturbation(classifier=classifier, eps=0.2, verbose = False)
    imgs_adv = attack.generate(x=Tensor.cpu(imgs).numpy())

    #calculate accuracy of adversarial images
    predictions = model(torch.from_numpy(imgs_adv).to("cuda"))
    n_correct = 0
    i= 0
    for prediction in predictions:
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs_adv)
    print("Accuracy on universal adversarial test examples: {}%".format(accuracy * 100))
    return accuracy*100

#pgd attack
def adversial_toolbox_pgd(model):
    classifier = PyTorchClassifier(
        model=model,
        #clip_values=(min_pixel_value, max_pixel_value),
        loss=torch.nn.CrossEntropyLoss(),
        optimizer=torch.optim.SGD(model.parameters(), lr=1e-3),
        input_shape=(1, 26, 26),
        nb_classes=10,
        device_type="cuda",
    )

    #produce adversarials
    attack = ProjectedGradientDescentPyTorch(estimator=classifier, eps=0.2)
    imgs_adv = attack.generate(x=Tensor.cpu(imgs).numpy())

    #calculate accuracy of adversarial images
    predictions = model(torch.from_numpy(imgs_adv).to("cuda"))
    n_correct = 0
    i= 0
    avg_certainty = 0
    for prediction in predictions:
        certainty = torch.nn.functional.softmax(prediction,dim = 0).max().item()
        avg_certainty += certainty
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs_adv)
    print("Accuracy on pgd adversarial test examples: {}%".format(accuracy * 100))
    print("Average pgd Certainty: {}%".format(avg_certainty/i*100))
    return accuracy*100

#wasserstein attack
def adversial_toolbox_wasserstein(model):
    classifier = PyTorchClassifier(
        model=model,
        #clip_values=(min_pixel_value, max_pixel_value),
        loss=torch.nn.CrossEntropyLoss(),
        optimizer=torch.optim.SGD(model.parameters(), lr=1e-3),
        input_shape=(1, 26, 26),
        nb_classes=10,
        device_type="cuda",
    )

    #produce adversarials
    attack = Wasserstein(estimator=classifier, eps=0.1, conjugate_sinkhorn_max_iter=100, max_iter = 16, batch_size=8)
    imgs_adv = attack.generate(x=Tensor.cpu(imgs).numpy())

    #calculate accuracy of adversarial images
    predictions = model(torch.from_numpy(imgs_adv).to("cuda"))
    n_correct = 0
    i= 0
    avg_certainty = 0
    for prediction in predictions:
        certainty = torch.nn.functional.softmax(prediction,dim = 0).max().item()
        avg_certainty += certainty
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs_adv)
    print("Accuracy on wasserstein adversarial test examples: {}%".format(accuracy * 100))
    print("Average wasserstein Certainty: {}%".format(avg_certainty/i*100))
    return accuracy*100


#newtonfool attack
def adversial_toolbox_newton(model):
    classifier = PyTorchClassifier(
        model=model,
        #clip_values=(min_pixel_value, max_pixel_value),
        loss=torch.nn.CrossEntropyLoss(),
        optimizer=torch.optim.SGD(model.parameters(), lr=1e-3),
        input_shape=(1, 26, 26),
        nb_classes=10,
        device_type="cuda",
    )

    #produce adversarials
    attack = NewtonFool(classifier=classifier)
    imgs_adv = attack.generate(x=Tensor.cpu(imgs).numpy())

    #calculate accuracy of adversarial images
    predictions = model(torch.from_numpy(imgs_adv).to("cuda"))
    n_correct = 0
    i= 0
    avg_certainty = 0
    for prediction in predictions:
        certainty = torch.nn.functional.softmax(prediction,dim = 0).max().item()
        avg_certainty += certainty
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs_adv)
    print("Accuracy on NewtonFool adversarial test examples: {}%".format(accuracy * 100))
    print("Average NewtonFool Certainty: {}%".format(avg_certainty/i*100))
    return accuracy*100

#elastic Net attack
def adversial_toolbox_elastic(model):
    classifier = PyTorchClassifier(
        model=model,
        #clip_values=(min_pixel_value, max_pixel_value),
        loss=torch.nn.CrossEntropyLoss(),
        optimizer=torch.optim.SGD(model.parameters(), lr=1e-3),
        input_shape=(1, 26, 26),
        nb_classes=10,
        device_type="cuda",
    )

    #produce adversarials
    attack = ElasticNet(classifier=classifier)
    imgs_adv = attack.generate(x=Tensor.cpu(imgs).numpy())

    #calculate accuracy of adversarial images
    predictions = model(torch.from_numpy(imgs_adv).to("cuda"))
    n_correct = 0
    i= 0
    avg_certainty = 0
    for prediction in predictions:
        certainty = torch.nn.functional.softmax(prediction,dim = 0).max().item()
        avg_certainty += certainty
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs_adv)
    print("Accuracy on ElasticNet adversarial test examples: {}%".format(accuracy * 100))
    print("Average ElasticNet Certainty: {}%".format(avg_certainty/i*100))
    return accuracy*100



#run attacks and save values
benign_standard_acc = test_accuracy(model_standard_trained)
untargeted_standard_acc = adversial_toolbox_untargeted(model_standard_trained)
targeted_standard_acc = adversial_toolbox_targeted(model_standard_trained)
universal_standard_acc = adversial_toolbox_universal(model_standard_trained)
pgd_standard_acc = adversial_toolbox_pgd(model_standard_trained)
wasserstein_standard_acc = adversial_toolbox_wasserstein(model_standard_trained)
newton_standard_acc = adversial_toolbox_newton(model_standard_trained)
elastic_standard_acc = adversial_toolbox_elastic(model_standard_trained)

#run attacks and save values
benign_optimized_acc = test_accuracy(model_adversarial_trained)
untargeted_optimized_acc = adversial_toolbox_untargeted(model_adversarial_trained)
targeted_optimized_acc = adversial_toolbox_targeted(model_adversarial_trained)
universal_optimized_acc = adversial_toolbox_universal(model_adversarial_trained)
pgd_optimized_acc = adversial_toolbox_pgd(model_adversarial_trained)
wasserstein_optimized_acc = adversial_toolbox_wasserstein(model_adversarial_trained)
newton_optimized_acc = adversial_toolbox_newton(model_adversarial_trained)
elastic_optimized_acc = adversial_toolbox_elastic(model_adversarial_trained)


#accuracy plot
names = ['benign', 'untargeted FGM', 'targeted FGM', 'Universal Perturbation', 'pgd', 'wasserstein', 'newtonFool', 'elasticNet']
standardValues = [benign_standard_acc, untargeted_standard_acc, targeted_standard_acc, universal_standard_acc, pgd_standard_acc, wasserstein_standard_acc, newton_standard_acc, elastic_standard_acc]
optimizedValues = [benign_optimized_acc, untargeted_optimized_acc, targeted_optimized_acc, universal_optimized_acc, pgd_optimized_acc, wasserstein_optimized_acc, newton_optimized_acc, elastic_optimized_acc]

#standardValues = [98.4375,10.9375,53.125,59.375,1.5625,72.65625,11.71875,1.5625] #Values from command line after crash during plotting
#optimizedValues = [100,86.71875,95.3125,99.21875,41.40625,73.4375,25.78125,0]

x = np.arange(len(names))  
width = 0.35  # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(x - width/2, standardValues, width, label='StandardTrained')
rects2 = ax.bar(x + width/2, optimizedValues, width, label='AdversarialTrained')
ax.set_ylabel('Accuracy')
ax.set_xticks(x, names)
ax.legend()

ax.bar_label(rects1, padding=3)
ax.bar_label(rects2, padding=3)

fig.tight_layout()
plt.show()

#plt.figure(figsize=(15,10))
#plt.subplot(131)
#plt.bar(names, values)
#plt.ylabel('Accuracy')
#plt.show()










