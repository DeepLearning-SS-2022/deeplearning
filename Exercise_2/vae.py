# Based on https://github.com/pytorch/examples/tree/master/vae

import os.path

import numpy as np
import torch.utils.data
import torchvision.utils
from torch import nn, optim
from torch.nn import functional
from torchvision import datasets, transforms
import umap
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import cv2

class VAE(nn.Module):
    def __init__(self, input_shape, device, width, latent_dimensionality):
        super(VAE, self).__init__()
        self.input_shape = input_shape
        self.n_input_values = np.prod(input_shape).item()
        self.device = device
        self.width = width
        self.latent_dimensionality = latent_dimensionality

        # Define encoding layers.
        self.input_encoder = nn.Linear(self.n_input_values, self.width)
        self.mean_encoder = nn.Linear(self.width, self.latent_dimensionality)
        self.var_encoder = nn.Linear(self.width, self.latent_dimensionality)

        # Define decoding layers.
        self.latent_decoder = nn.Linear(self.latent_dimensionality, self.width)
        self.final_decoder = nn.Linear(self.width, self.n_input_values)

    def encode(self, original):
        encoded_input = functional.relu(self.input_encoder(original))
        return self.mean_encoder(encoded_input), self.var_encoder(encoded_input)

    def reparameterize(self, mean, log_variance):
        deviation = torch.exp(0.5 * log_variance)
        noise = torch.randn_like(deviation, device=self.device)
        return mean + noise * deviation

    def decode(self, latent):
        decoded_latent = functional.relu(self.latent_decoder(latent))
        final = torch.sigmoid(self.final_decoder(decoded_latent))
        return final.view(-1, *self.input_shape)

    def forward(self, original):
        mean, log_variance = self.encode(original.view(-1, self.n_input_values))
        latent = self.reparameterize(mean, log_variance)
        decoded = self.decode(latent)
        return decoded, mean, log_variance

    def variational_loss_function(
        self, decoded, original, mean, log_variance
    ) -> torch.Tensor:
        entropy = functional.binary_cross_entropy(
            decoded, original.view(-1, *self.input_shape), reduction="sum"
        )

        divergence = -0.5 * torch.sum(
            1 + log_variance - mean.pow(2) - log_variance.exp()
        )

        return entropy + divergence


def train_and_eval(model, train_loader, fixed_batch, optimizer, batch_size, width, latent_dimensionality, n_epochs, learning_rate, out_path, image_extension, max_saves_per_epoch, device,):
    image_outpath = out_path + "/images"
    text_outpath = out_path + "/texts"
    model_outpath = out_path + "/models"

    # Prepare random latent to observe progress.
    fixed_latent = noise = torch.randn(
        [batch_size, latent_dimensionality], device=device
    )

    # Process epochs.
    n_batches = len(train_loader)
    for i_epoch in range(n_epochs):
        model.train()
        n_save_batches = int((1 - i_epoch / n_epochs) * max_saves_per_epoch + 1)
        save_interval = int(n_batches / n_save_batches)
        # Train model.
        for i_batch, (batch, _) in enumerate(train_loader):
            batch = batch.to(device)
            optimizer.zero_grad()
            reconstruction, mean, log_variance = model(batch)
            loss = model.variational_loss_function(
                reconstruction, batch, mean, log_variance
            )
            loss.backward()
            optimizer.step()

            if i_batch % save_interval == 0:
                # Print progress.
                print(f"{i_epoch = }, {i_batch = }")

                # Process fixed batch.
                model.eval()
                reconstruction, _, _ = model(fixed_batch)

                grid = torchvision.utils.make_grid(reconstruction)
                path = os.path.join(
                    image_outpath,
                    f"reconstruction-{i_epoch:0{len(str(n_epochs))}}"
                    f"-{i_batch:0{len(str(n_batches))}}"
                    f".{image_extension}",
                )
                torchvision.utils.save_image(grid, path)
                with open(os.path.join(text_outpath, f"reconstruction-{i_epoch:0{len(str(n_epochs))}}"f"-{i_batch:0{len(str(n_batches))}}"".txt"), "w+") as file:
                    file.write(f"reconstruction_error: {loss}")
                # Process fixed latent.
                decoded = model.decode(fixed_latent)

                grid = torchvision.utils.make_grid(decoded)
                path = os.path.join(
                    image_outpath,
                    f"random-{i_epoch:0{len(str(n_epochs))}}"
                    f"-{i_batch:0{len(str(n_batches))}}"
                    f".{image_extension}",
                )
                torchvision.utils.save_image(grid, path)
    torch.save(model.state_dict(), model_outpath + "/model1.pt")


def linear_combination(vec1, vec2, alpha):
    return vec1*alpha + vec2 * (1-alpha)


def show_latent_space(model, img1, img2, step_size=10, reparameterize=False):
    images = []

    mean1, var1 = model.encode(img1)
    mean2, var2 = model.encode(img2)

    if reparameterize:
        enc1 = model.reparameterize(mean1, var1)
        enc2 = model.reparameterize(mean2, var2)
    else:
        enc1 = mean1
        enc2 = mean2

    for alpha in range(0, 100+step_size, step_size):
        decoding = model.decode(linear_combination(enc1, enc2, alpha/100))
        images.append(decoding[0][0].cpu().detach().numpy())
        img = cv2.normalize(images[-1], None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)
        plt.imshow(img)
        plt.savefig(f'output/images/lin_comb/{alpha}.png')
        plt.clf()

def visualize_umap(model, data):
    from sklearn.datasets import load_digits
    from sklearn.model_selection import train_test_split
    from sklearn.preprocessing import StandardScaler
    import matplotlib.pyplot as plt
    import seaborn as sns
    import pandas as pd

    encodings = []
    labels =  []

    for batch_features, batch_labels in data:
        mean, var = model.encode(batch_features.cuda().view(-1, model.n_input_values))
        encodings.append(mean.cpu().detach().numpy())
        labels.append(batch_labels.cpu().detach().numpy())
    encodings = np.vstack(encodings)
    labels = np.concatenate(labels)
    #possible_labels = np.unique(labels)
    colors = [sns.color_palette()[x] for x in labels]
    encodings_pd = pd.DataFrame(encodings, columns=[i for i in range(encodings.shape[1])])
    reducer = umap.UMAP()
    embedding = reducer.fit_transform(encodings_pd)

    plt.scatter(
        embedding[:, 0],
        embedding[:, 1],
        c=colors)
    plt.gca().set_aspect('equal', 'datalim')
    plt.title('UMAP projection of the dataset', fontsize=24)
    plt.savefig("output/images/umap.png")



if __name__ == "__main__":

    # configs
    batch_size = 128
    n_epochs = 20
    learning_rate = 1e-3
    output_path = "output"
    image_extension = "png"
    latent_dimensionality = 20
    width = 400

    # Select GPU if available.
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

    # Set seed.
    torch.manual_seed(0)

    # Prepare data.
    train_loader = torch.utils.data.DataLoader(
        datasets.FashionMNIST(
            "D:/datasets/FashionMNIST", train=True, download=True, transform=transforms.ToTensor()
        ),
        batch_size=batch_size,
        shuffle=True,
    )
    test_loader = torch.utils.data.DataLoader(
        datasets.FashionMNIST(
            "D:/datasets/FashionMNIST", train=False, download=True, transform=transforms.ToTensor()
        ),
        batch_size=batch_size,
        shuffle=True,
    )

    # Prepare batch to observe progress.
    fixed_batch, _ = next(iter(test_loader))
    grid = torchvision.utils.make_grid(fixed_batch)
    os.makedirs(output_path, exist_ok=True)
    path = os.path.join(output_path, f"original.{image_extension}")
    torchvision.utils.save_image(grid, path)
    fixed_batch = fixed_batch.to(device)
    """
    # Prepare model.
    model = VAE(
        input_shape=fixed_batch.shape[1:],
        width=width,
        latent_dimensionality=latent_dimensionality,
        device=device,
    ).to(device)
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)
    
    train_and_eval(
        model=model,
        train_loader=train_loader,
        fixed_batch=fixed_batch,
        optimizer=optimizer,
        batch_size=batch_size,
        width=width,
        latent_dimensionality=latent_dimensionality,
        n_epochs=n_epochs,
        learning_rate=learning_rate,
        out_path=output_path,
        image_extension=image_extension,
        max_saves_per_epoch=20,
        device="cuda"
    )
    """

    model = VAE(
        input_shape=fixed_batch.shape[1:],
        width=width,
        latent_dimensionality=latent_dimensionality,
        device=device,
    ).to(device)
    model.load_state_dict(torch.load("output/models/model1.pt"))
    """
    img1 = next(iter(test_loader))[0][0:1].cuda().view(-1)
    img2 = next(iter(test_loader))[0][1:2].cuda().view(-1)

    show_latent_space(model, img1, img2)
    """
    visualize_umap(model, data=iter(test_loader))


