import torch
import torchvision
from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor
import numpy as np
from torch import Tensor
import matplotlib.pyplot as plt

from art.attacks.evasion import FastGradientMethod
from art.attacks.evasion import UniversalPerturbation
from art.estimators.classification import PyTorchClassifier

test_data = datasets.FashionMNIST(
	root="data",
	train=False,
	download=True,
	transform=ToTensor(),
)

test_data_loader = DataLoader(test_data, batch_size=64) 

class FashionNetwork(nn.Module):
    def __init__(self):
        super(FashionNetwork, self).__init__()

        self.conv_relu_stack = nn.Sequential(
            nn.Conv2d(1, 5, kernel_size=3, stride=2, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),
        )
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(7*7*5, 200),
            nn.ReLU(),
            nn.Linear(200, 10),
        )

    def forward(self, x):
        outs = self.conv_relu_stack(x)
        logits = self.linear_relu_stack(outs.view(outs.size(0), -1))
        return logits


model = torch.load("model") #import model from exercise 1
model.eval()
input_shape=(26,26)
cuda = torch.device('cuda')

#tutorial attack from the exercises, only works with batch size 1, not used for results
def image_to_tensor(input_array):
    # Normalize input.
    input_array = input_array - [0.485, 0.456, 0.406]
    input_array = input_array / [0.229, 0.224, 0.225]
 
    # Turn from (width, height, channel) into (batch, channel, width, height) and convert to tensor.
    input_tensor = torch.Tensor((input_array.transpose([2, 0, 1]))[None, ...])
 
    return input_tensor

sample = next(iter(test_data_loader))
imgs, lbls = sample
imgs = imgs.to("cuda")

original_label = model(imgs)[0].argmax().item()
target_tensor = torch.tensor([original_label], dtype=torch.long, device=cuda)

def tutorial_attack():
    prediction_tensor = model(imgs)
    loss = torch.nn.CrossEntropyLoss()
    loss_value = loss(prediction_tensor, target_tensor)
    loss_value.backward()

    attack_scale = 0.5 * 0.5
    new_label = original_label
    while new_label == original_label:
        attack_scale *= 2.0
        imgs.data += attack_scale
 
        # Clamp modified input such that it corresponds to an image.
        imgs.data[:, 0, :, :].clamp_(-0.485 / 0.229, (1 - 0.485) / 0.229)
 
        # Pass modified input through network and determine “winning” label.
        prediction_tensor = model(imgs)[0]
        new_label = prediction_tensor.argmax().item()

	
    certainty_tensor = torch.nn.functional.softmax(prediction_tensor)
    print(certainty_tensor)
    print(new_label)
    print(original_label)

#baseline accuracy test
def test_accuracy():
    #calculate accuracy of test images
    predictions = model(imgs)
    n_correct = 0
    i= 0
    for prediction in predictions:
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs)
    print("Accuracy on benign test examples: {}%".format(accuracy * 100))
    return accuracy*100

#targeted attack
def adversial_toolbox_targeted():
    classifier = PyTorchClassifier(
        model=model,
        #clip_values=(min_pixel_value, max_pixel_value),
        loss=torch.nn.CrossEntropyLoss(),
        optimizer=torch.optim.SGD(model.parameters(), lr=1e-3),
        input_shape=(1, 26, 26),
        nb_classes=10,
        device_type="cuda",
    )
    
    #produce adversarials
    attack = FastGradientMethod(estimator=classifier, eps=0.2, targeted = True)
    imgs_adv = attack.generate(x=Tensor.cpu(imgs).numpy(),y=Tensor([1 for l in lbls])) #attack for all labels = 1

    #calculate accuracy of adversarial images
    predictions = model(torch.from_numpy(imgs_adv).to("cuda"))
    n_correct = 0
    i= 0
    for prediction in predictions:
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs_adv)
    print("Accuracy on targeted adversarial test examples: {}%".format(accuracy * 100))
    return accuracy*100

#untargted attack
def adversial_toolbox_untargeted():
    classifier = PyTorchClassifier(
        model=model,
        #clip_values=(min_pixel_value, max_pixel_value),
        loss=torch.nn.CrossEntropyLoss(),
        optimizer=torch.optim.SGD(model.parameters(), lr=1e-3),
        input_shape=(1, 26, 26),
        nb_classes=10,
        device_type="cuda",
    )

    #produce adversarials
    attack = FastGradientMethod(estimator=classifier, eps=0.2)
    imgs_adv = attack.generate(x=Tensor.cpu(imgs).numpy())

    #calculate accuracy of adversarial images
    predictions = model(torch.from_numpy(imgs_adv).to("cuda"))
    n_correct = 0
    i= 0
    avg_certainty = 0
    for prediction in predictions:
        certainty = torch.nn.functional.softmax(prediction,dim = 0).max().item()
        avg_certainty += certainty
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs_adv)
    print("Accuracy on untargeted adversarial test examples: {}%".format(accuracy * 100))
    print("Average untargeted Certainty: {}%".format(avg_certainty/i*100))
    return accuracy*100, avg_certainty/i*100

#modified attack for higher certainty
def adversial_toolbox_high_certainty():
    classifier = PyTorchClassifier(
        model=model,
        #clip_values=(min_pixel_value, max_pixel_value),
        loss=torch.nn.CrossEntropyLoss(),
        optimizer=torch.optim.SGD(model.parameters(), lr=1e-3),
        input_shape=(1, 26, 26),
        nb_classes=10,
        device_type="cuda",
    )

    #produce adversarials
    attack = FastGradientMethod(estimator=classifier, eps=0.2)
    imgs_adv = attack.generate(x=Tensor.cpu(imgs).numpy())
    
    #get new labels from the adversarial images
    newlbls = [] 
    predictions = model(torch.from_numpy(imgs_adv).to("cuda"))
    for prediction in predictions:
        newlbls.append(prediction.argmax().item())

    #targeted attack on new labels to increase certainty
    attack = FastGradientMethod(estimator=classifier, eps=0.2, targeted = True)
    imgs_adv = attack.generate(x=imgs_adv,y=Tensor(newlbls))

    #calculate accuracy of adversarial images
    predictions = model(torch.from_numpy(imgs_adv).to("cuda"))
    n_correct = 0
    i= 0
    avg_certainty = 0
    for prediction in predictions:
        certainty = torch.nn.functional.softmax(prediction,dim = 0).max().item()
        avg_certainty += certainty
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs_adv)
    print("Accuracy on high certainty adversarial test examples: {}%".format(accuracy * 100))
    print("Average optimized Certainty: {}%".format(avg_certainty/i*100))
    return accuracy*100, avg_certainty/i*100

#universal attack
def adversial_toolbox_universal():
    classifier = PyTorchClassifier(
        model=model,
        #clip_values=(min_pixel_value, max_pixel_value),
        loss=torch.nn.CrossEntropyLoss(),
        optimizer=torch.optim.SGD(model.parameters(), lr=1e-3),
        input_shape=(1, 26, 26),
        nb_classes=10,
        device_type="cuda",
    )

    #produce adversarials
    attack = UniversalPerturbation(classifier=classifier, eps=0.2, verbose = False)
    imgs_adv = attack.generate(x=Tensor.cpu(imgs).numpy())

    #calculate accuracy of adversarial images
    predictions = model(torch.from_numpy(imgs_adv).to("cuda"))
    n_correct = 0
    i= 0
    for prediction in predictions:
        if prediction.argmax().item() == lbls[i]:
            n_correct += 1
        i += 1
    accuracy = n_correct / len(imgs_adv)
    print("Accuracy on universal adversarial test examples: {}%".format(accuracy * 100))
    return accuracy*100

#run attacks and save values
benign_acc = test_accuracy()
untargeted_acc, unoptimized_cer = adversial_toolbox_untargeted()
targeted_acc = adversial_toolbox_targeted()
highcer_acc, optimized_cer = adversial_toolbox_high_certainty()
universal_acc = adversial_toolbox_universal()

#accuracy plot
names = ['benign', 'untargeted', 'targeted', 'universal']
values = [benign_acc, untargeted_acc, targeted_acc, universal_acc]
plt.figure(figsize=(15,10))
plt.subplot(131)
plt.bar(names, values)
plt.ylabel('Accuracy')
plt.show()

#certainty plot
names = ['unoptimized_certainty', 'optimized_certainty']
values = [unoptimized_cer, optimized_cer]
plt.figure(figsize=(15,10))
plt.subplot(132)
plt.bar(names, values)
plt.ylabel('Accuracy')
plt.show()










